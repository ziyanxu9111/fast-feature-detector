function [cand_lower,cand_higher] = fast_helper(keypoint,...
    im, ...
    shifted, ...
    T, ...
    x, ...
    y, ...
    cand_lower, ...
    cand_higher, ...
    num_cand)
%helper function to handle repeated operations in fast detector
%   im: original image in grayscale, uint8
%   shifted: shifted image, uint8
%   T: threshold, int
%   x: list of candidate x coordinate, [double]
%   y: list of candidate y coordinate. [double]
%   keypoint: index of keypoint, int (1 - 16)
%   cand_lower: intensity difference for each candidate, 1 if
%               keypoint has lower intensity
%   cand_higher: intensity difference for each candidate, 1 if
%               keypoint has higher intensity
%   num_cand: number of candidate points

diff = shifted - im;
higher = diff > T;
diff = im - shifted;
lower = diff > T;

for i = 1 : num_cand
    x_cord = x(i);
    y_cord = y(i);
    cand_higher(i, keypoint) = higher(x_cord, y_cord);
    cand_higher(i, keypoint + 16) = higher(x_cord, y_cord);
    cand_lower(i, keypoint) = lower(x_cord, y_cord);
    cand_lower(i, keypoint + 16) = lower(x_cord, y_cord);
end
end


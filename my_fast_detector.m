function [points] = my_fast_detector(image,threshold)
%Perform FAST corner detection with high speed test, 
% without nonmax suppression
%   INPUTS:
%   image: original image in grayscale, uint8
%   threshold: threshold for two pixels to be considered different, 
%       default 40
%   OUTPUTS:
%   list of cornerPoints objects

corners_x = [];
corners_y = [];
if ~exist('threshold', 'var')
    T = 40;
    fprintf('Default threshold: 40.');
else
    T = threshold;
end

im = image;

%% High speed test
% for keypoint 1, 9, 5, 13, check if at least 3 are brighter or darker

higher = zeros(size(im));
lower = zeros(size(im));

% 1
shifted = circshift(im, 3, 1);
diff = shifted - im;
higher = higher + (diff > T);
diff = im - shifted;
lower = lower + (diff > T);

% 9
shifted = circshift(im, -3, 1);
diff = shifted - im;
higher = higher + (diff > T);
diff = im - shifted;
lower = lower + (diff > T);

% 5
shifted = circshift(im, -3, 2);
diff = shifted - im;
higher = higher + (diff > T);
diff = im - shifted;
lower = lower + (diff > T);

% 13
shifted = circshift(im, 3, 2);
diff = shifted - im;
higher = higher + (diff > T);
diff = im - shifted;
lower = lower + (diff > T);

candidate = (higher >= 3) + (lower >= 3);

num_cand = nnz(candidate);

% holder for intensity difference for each candidate at each keypoint
% 1 if higher/lower, 0 if not
cand_higher = zeros(num_cand, 32);
cand_lower = zeros(num_cand, 32);

% NOTE:
% list is repeated once to handle run of 1's across 16--1
% eg: 
% 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
% 1 1 1 1 1 0 0 0 0  1  1  1  1  1  1  1
% should be a run of 12 1's but will not be treated as continues area
% 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25...
% 1 1 1 1 1 0 0 0 0  1  1  1  1  1  1  1  1  1  1  1  1  0  0  0  0...
% contains a run of 12 1's which will be treated as continuous area

[x, y] = find(candidate);

%%
shifts = [[3, 0]; ...
    [3, -1]; ...
    [2, -2]; ...
    [1, -3]; ...
    [0, -3]; ...
    [-1, -3]; ...
    [-2, -2]; ...
    [-3, -1]; ...
    [-3, 0]; ...
    [-3, 1]; ...
    [-2, 2]; ...
    [-1, 3]; ...
    [0, 3]; ...
    [1, 3]; ...
    [2, 2]; ...
    [3, 1]];
%%
for i = 1 : 16
    shifted = circshift(im, shifts(i,:));
    [cand_lower, cand_higher] = fast_helper(i, im, shifted, T, x, y,  ...
        cand_lower, cand_higher, num_cand);
end
%% Process record and determine corner list
for i = 1 : num_cand
    
    l = cand_lower(i, :);
    h = cand_higher(i, :);
    
    % determine the area of the longest run of 1's
    % method adapted from https://www.mathworks.com/matlabcentral/answers/281373-longest-sequence-of-1s#answer_219724
    hh = nonzeros((cumsum(~h)).*h);
    if isempty(hh);
        hh = 0;
    else
        hh = max(accumarray(hh, 1));
    end
    
    ll = nonzeros((cumsum(~l)).*l);
    if isempty(ll)
        ll = 0;
    else
        ll = max(accumarray(ll, 1));
    end
    
    if ll >=12 || hh >= 12
        corners_x = [corners_x ; x(i)];
        corners_y = [corners_y ; y(i)];
    end
end

% return values
points = cornerPoints([corners_y corners_x]);
